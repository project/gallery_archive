<?php

/**
 * @file
 * "Download Gallery" tab building function.
 *
 * This file builds the "Download Gallery" tab which appears on image gallery
 * node pages.
 */

/**
 * Archives gallery images
 */
function gallery_archive_gallery($node) {

  // First, define some variables
  $node_id = $node->nid; // We love nids
  $directory_name = $node_id . '-' . time(); // nid + Current UNIX timestamp (must be unique)
  $tmp_directory = $_SERVER['DOCUMENT_ROOT'] . '/' . file_directory_temp() . '/' . $directory_name;
  $image_base_path = $_SERVER['DOCUMENT_ROOT'] . '/'; // Path to docroot
  $archive_name = "$node_id-archive"; // Used for name of final archive
  $archive_path = file_directory_path() . "/gallery-archives/" . $archive_name; // Used for building a link

  // @TODO - Before creating a new archive, check if (a) the archive already
  // exists, and (b) if the archive is more than a week old. We'll need to
  // come up with a better solution for stale data here, though... but I want
  // to be efficient!

  if (!file_exists($archive_path)) {

    // Create a new temporary directory to store the images
    if (!mkdir($tmp_directory, 0775)) {
      return "<div>" . t("Error: Temporary directory could not be created. Please check your file system.") . "</div>";
    }

    // SQL query to get backreference nodes (photo content type + cck
    // nodereference field)

    // @TODO - Views gallery module uses:
    // content_type_gallery_image - db table (NOT content_type_*photo*)
    // field_gallery_image - image field
    // field_gallery - node reference field (NOT field_gallery_nid)

    // Set up some variables for our SQL query
    $cck_image_table = check_plain('content_type_' . variable_get('gallery_archive_image_content_type', 'gallery_image')); // DB table for images
    $gallery_nr_field = variable_get('gallery_archive_gallery_nodereference_field', 'field_gallery') . "_nid"; // Gallery nodereference field
    $sql = "SELECT nid FROM {$cck_image_table} WHERE %s = '%s'";
    // Create an array for results of query
    $backreference_array = array();
    // Find references with db_query
    $result = db_query(db_rewrite_sql($sql), $gallery_nr_field, $node_id);

    $i = 0; // Counter

    // Get the results and put into $backreference_array
    while ($data = db_fetch_object($result)) {
      $current_linked_node = node_load($data->nid);
      // If we need it at a later time
      // $backreference_array[$i]['nid'] = $current_linked_node->nid;

      // Grab path to full-resolution image
      $image_to_copy = $image_base_path . $current_linked_node->field_gallery_image[0]['filepath'];

      // Copy image to $tmp_directory
      file_copy($image_to_copy, $tmp_directory, FILE_EXISTS_REPLACE);
      $i++;
    }

    // @TODO - If PHP is compiled with --enable-zip, create a .zip archive...
    // Opt 1: http://davidwalsh.name/create-zip-php
    // Opt 2: http://www.webandblog.com/hacks/zip-a-folder-on-the-server-with-php/

    // Create the archive using the gallery_archive_tgs_archive_dir() function
    gallery_archive_tgz_archive_dir($tmp_directory, $archive_name, TRUE);
  }

  // Check to see if the Archive actually exists, has content.
  $archive_size = filesize($archive_path . '.tar.gz');

  if ($archive_size > 0) {

    $archive_size = _gallery_archive_format_bytes($archive_size); // Make filesize human-readable

    // Render the Download Gallery page.
    $rendered_page .= "<div>" . t("Click on the download link below to download this gallery:") . "</div>";
    $link_text = t("Download this gallery") . " ($archive_size)";
    $rendered_page .= l($link_text, $archive_path . '.tar.gz');
  }

  else {

    $rendered_page .= "<div>" . t("Error: No archive has been created. Please check your settings.") . "</div>";

  }
  
  return $rendered_page;

}
