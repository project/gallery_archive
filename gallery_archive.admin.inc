<?php

/**
 * @file
 * Form-building function for Gallery Archive admin settings page
 *
 * This file builds the Gallery Archive settings page.
 *
 * @todo - Set this to use a select-list instead of textfield. Get an array
 * of content types and of fields, and allow the user to select rather than
 * type them in directly.
 */

/**
 * Implementation of hook_form().
 */
function gallery_archive_settings_form(&$form_state) {

  // Set up options for gallery types.
  $gallery_types = array(
    'views',
    'node',
  );

  // Set up options for gallery types.
  $gallery_archive_deletion_frequeny_options = array(
    'when cron runs',
    'never',
  );

  // @todo - Allow users to use nodes-as-galleries...
  $form['gallery_archive_gallery_type'] = array(
    '#type' => 'select',
    '#title' => 'Content Type',
    '#options' => $gallery_types,
    '#disabled' => TRUE,
    '#description' => t('Select whether you\'re using views_gallery-provided image galleries, or nodes-as-galleries.'),
    '#default_value' => variable_get('gallery_archive_gallery_type', 'views'),
  );

  $form['gallery_archive_deletion_frequency'] = array(
    '#type' => 'select',
    '#title' => 'Deletion Frequency',
    '#options' => $gallery_archive_deletion_frequeny_options,
    '#description' => t('To save space on your server, you can have the gallery archives folder empty on cron runs.'),
    '#default_value' => variable_get('gallery_archive_deletion_frequency', 'never'),
  );

  $form['gallery_archive_gallery_content_type'] = array(
    '#type' => 'textfield',
    '#title' => 'Gallery Content Type',
    '#description' => t('Enter the content type of your galleries (the \'Download Gallery\' tab will be added to this content type).'),
    '#default_value' => variable_get('gallery_archive_gallery_content_type', 'gallery'),
  );

  $form['gallery_archive_image_content_type'] = array(
    '#type' => 'textfield',
    '#title' => 'Image Content Type',
    '#description' => t('Enter the content type of your images (which refer to the gallery content type, set above).'),
    '#default_value' => variable_get('gallery_archive_image_content_type', 'image'),
  );

  $form['gallery_archive_gallery_nodereference_field'] = array(
    '#type' => 'textfield',
    '#title' => 'Gallery Nodereference Field',
    '#description' => t('Enter the field name for the nodereference field used by your image content type to refer to your gallery content type.'),
    '#default_value' => variable_get('gallery_archive_gallery_nodereference_field', 'field_gallery'),
  );

  // @todo - Allow users to change image field...
  $form['gallery_archive_image_field'] = array(
    '#type' => 'textfield',
    '#title' => 'Image Field',
    '#disabled' => TRUE,
    '#description' => t('Enter the field name for the field that contains the images displayed in your image gallery. Not configurable yet.'),
    '#default_value' => variable_get('gallery_archive_image_field', 'field_gallery_image'),
  );
  
  // DISABLE THIS UNTIL WORKING...

  // @todo - Example for future use, when we have an array of content types...
  // $form['gallery_archive_content_type'] = array(
  //   '#type' => 'select',
  //   '#title' => 'Content Type',
  //   '#options' => $content_types,
  //   '#description' => t('Select the content type of your galleries (the \'Download Gallery\' tab will be added to this content type).'),
  //   '#default_value' => variable_get('gallery_archive_content_type', 'gallery'),
  // );

  return system_settings_form($form);
}
