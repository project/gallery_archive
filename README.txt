
Gallery Archive Module - README.txt

The Gallery Archive module allows users with the appropriate permissions to
download an archive of a particular image gallery, with the original-size
images in a .tar.gz (or, in the future, .zip) file.

The module currently only works with image galleries created by the
views_gallery module (http://drupal.org/project/views_gallery), but will soon
be adapted to also work with nodes-as-galleries or custom cck gallery types.

More information about this module, including the latest release, can be found
at http://drupal.org/project/gallery_archive.

IMPORTANT NOTES:

- Module settings can be changed on the Gallery Archive settings page, located
  at admin/settings/gallery_archive (under "Site configuration").

- Gallery archives are never removed by this module by default. You can change
  this setting on the settings page, under the 'Deletion Frequency' option. If
  you need to change the frequency of the deletion (for instance, if galleries
  often have new pictures added, and you'd like the archive to be rebuilt
  every hour), you can use a module like Elysia Cron to set a different
  interval for the Gallery Archive module's cron run.

- This module has only been tested with the Views Gallery module so far, but
  is known to work well with custom configurations of views galleries as well.
  The module will include support for nodes-as-galleries as well, soon.